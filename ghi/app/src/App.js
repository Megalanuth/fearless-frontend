import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css'
import Nav from './nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route path="new"  element={<AttendConferenceForm />} />
            </Route>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="attendees" index element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
