function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col">    
        <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-body-secondary">
                    ${starts} - ${ends}
                </div>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // Figure out what to do when the response is bad
            throw new Error(`HTTP error, status = ${response.status}`);
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    } 
    catch (e) {
        // Figure out what to do if an error is raised
        console.error(e);
        const errorhtml = `
            <div class="alert alert-danger" role="alert">
                Error occured while fetching conference data: ${e.message}
            </div>
        `;
        const column = document.querySelector('.col');
        column.innerHTML += errorhtml;
    }

});
