window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      
      // Here, add the 'd-none' class to the loading icon
      const loadingIcon = document.getElementById("loading-conference-spinner");
      loadingIcon.classList.add("d-none");
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove("d-none");
    }
            // get the attendee form element by its id
            const formTag = document.getElementById('create-attendee-form');
            // add an event handler for the submit event
            formTag.addEventListener('submit', async (event) => {
                // prevent the browser default behavior from happening
                event.preventDefault();
                // Create a FormData object from the form
                const formData = new FormData(formTag);
                // Get a new object from the form data's entries
                const json = JSON.stringify(Object.fromEntries(formData));
                const attendeesUrl = 'http://localhost:8001/api/attendees/';
                // Create options for the fetch
                const fetchConfig = {
                    method: "POST",
                    body: json,
                    headers: {
                        "Content-Type": "application.json",
                    },
                };
                // Make the fetch using the await keyword to the URL
                const response = await fetch(attendeesUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newAttendee = await response.json();
                    // console.log(newAttendee);
                    formTag.classList.add("d-none");
                    const successTag = document.getElementById("success-message");
                    successTag.classList.remove("d-none");
                }
            });
  
  });